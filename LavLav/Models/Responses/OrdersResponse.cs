﻿using System;
using System.Collections.Generic;

namespace LavLav
{
	public class OrdersResponse
	{
		public List<Order> OrderList { get; set; }
	}
}
