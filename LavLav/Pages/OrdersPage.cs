﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace LavLav
{
	public class OrdersPage : BasePage
	{
		List<Order> Orders;
		public OrdersPage(List<Order> pOrders)
		{
			Title = "Pedidos Abertos";
			Orders = pOrders;
			LoadLayout(BuildLayout());
		}

		Layout BuildLayout()
		{
			var listView = new ListView
			{
				ItemsSource = Orders,
				ItemTemplate = new DataTemplate(typeof(WashCell)),
				VerticalOptions = LayoutOptions.FillAndExpand,
				SeparatorVisibility = SeparatorVisibility.None,
				HasUnevenRows = true, 
				RowHeight = 190,
				BackgroundColor = Color.Transparent
			};

			listView.ItemTapped += (sender, e) =>
			{
				var order = (Order)e.Item;
				if (order != null)
				{
					Navigation.PushAsync(new OrderPage(order));
				}
			};

			return new StackLayout
			{
				Padding = 10,
				Children = {
					listView
				}
			};
		}
}
}
