﻿using System;
namespace LavLav
{
	public class Addon : BaseModel
	{
		public string name { get; set; }
		public string description { get; set; }
		public string createdAt { get; set; }
		public object price { get; set; }
	}
}
