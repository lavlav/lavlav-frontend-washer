﻿using System;
namespace LavLav
{
	public class Address : BaseModel
	{
		public string description { get; set; }
		public string createdAt { get; set; }
		public string streetName { get; set; }
		public string streetNumber { get; set; }
		public string neighborhood { get; set; }
		public string zipCode { get; set; }
		public City city { get; set; }
		public string comments { get; set; }
	}

	public class Country : BaseModel
	{
		public string name { get; set; }
		public string createdAt { get; set; }
	}

	public class State : BaseModel
	{
		public string name { get; set; }
		public string abbr { get; set; }
		public string createdAt { get; set; }
		public Country country { get; set; }
	}

	public class City : BaseModel
	{
		public string name { get; set; }
		public string createdAt { get; set; }
		public State state { get; set; }
	}
}
