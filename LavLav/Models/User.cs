﻿using System;

namespace LavLav
{
	public class User : BaseModel
	{
		public String email { get; set; }
		public String name { get; set; }
		public string lastName { get; set; }
		public DateTime createdAt { get; set; }
		public String gender { get; set; }
		public String phoneNumber { get; set; }
		public String Token { get; set; }
		public string imageURL { get; set; }
		public int rating { get; set; }
		public String fullName { get { return name + " " + lastName; } }
	}
}

