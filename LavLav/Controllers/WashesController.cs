﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace LavLav
{
	public static class WashesController
	{
		static internal async Task<List<Order>> GetMyOrders()
		{
			var ordersResponse = await App.RequestHandler.GetMyOrders();
			return ordersResponse;
		}
		static internal List<Wash> GetWaitingWashesList()
		{
			//TODO WashesList
			List<Wash> washList = new List<Wash>();
			washList.Add(new Wash(07104,
								  "Lavagem Ecológica",
								  "Fiat Punto",
								  DateTime.Now,
								  "Rua Viçosa, 533, São Pedro, Belo Horizonte"));
			washList.Add(new Wash(08239,
								  "Lavagem Ecológica",
								  "Ford KA",
								  DateTime.Now,
								  "Rua Maçaroca, 666, Betânia, Belo Horizonte"));
			washList.Add(new Wash(01313,
								  "Enceramento",
								  "Kombi",
								  DateTime.Now,
								  "Rua das Flores, 235, Nova Suiça, Belo Horizonte"));

			return washList;
		}

		static internal async Task<List<Order>> GetAvailableOrders(User user)
		{
			var ordersResponse = await App.RequestHandler.GetAvailableOrders();
			//TODO WashesList
			List<Wash> washList = new List<Wash>();
			washList.Add(new Wash(07104,
								  "Lavagem Ecológica",
								  "Fiat Punto",
								  DateTime.Now,
								  "Rua Viçosa, 533, São Pedro, Belo Horizonte"));
			washList.Add(new Wash(08239,
								  "Lavagem Ecológica",
								  "Ford KA",
								  DateTime.Now,
								  "Rua Maçaroca, 666, Betânia, Belo Horizonte"));
			return ordersResponse;
		}

		static internal async Task<Response> ReserveOrder(int pOrderID, DateTime pScheduledAt)
		{
			var response = await App.RequestHandler.ReserveOrder(pOrderID, pScheduledAt);
			return response;
		}

		static internal async Task<bool> StartOrder(int pOrderID)
		{
			var response = await App.RequestHandler.StartOrder(pOrderID);
			return !response.message.Equals(Constants.Error);
		}

		static internal async Task<bool> FinishOrder(int pOrderID)
		{
			var success = await App.RequestHandler.FinishOrder(pOrderID);
			return success;
		}

		static internal async Task<bool> CancelOrder(int pOrderID)
		{
			var success = await App.RequestHandler.CancelOrder(pOrderID);
			return success;
		}

		static internal async Task<Order> GetOrder(int pOrderID)
		{
			var order = await App.RequestHandler.GetOrder(pOrderID);
			return order;
		}
	}
}
