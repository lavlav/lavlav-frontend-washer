﻿using System;
using System.Linq;
using LavLav.iOS;
using Xamarin.Auth;
using Xamarin.Forms;

[assembly: Dependency(typeof(CredentialsService))]
namespace LavLav.iOS
{
	public class CredentialsService : ICredentialsService
	{
		public string UserName
		{
			get
			{
				var account = AccountStore.Create().FindAccountsForService(Constants.AppName).FirstOrDefault();
				return (account != null) ? account.Username : null;
			}
		}

		public string Password
		{
			get
			{
				var account = AccountStore.Create().FindAccountsForService(Constants.AppName).FirstOrDefault();
				return (account != null) ? account.Properties["Password"] : null;
			}
		}

		public void SaveCredentials(string userName, string password)
		{
			return;
			//if (!string.IsNullOrWhiteSpace(userName) && !string.IsNullOrWhiteSpace(password))
			//{
			//	Account account = new Account
			//	{
			//		Username = userName
			//	};
			//	account.Properties.Add("Password", password);
			//	AccountStore.Create().Save(account, Constants.AppName);
			//}

		}

		public void DeleteCredentials()
		{
			return;
			//var account = AccountStore.Create().FindAccountsForService(Constants.AppName).FirstOrDefault();
			//if (account != null)
			//{
			//	AccountStore.Create().Delete(account, Constants.AppName);
			//}
		}

		public bool DoCredentialsExist()
		{
			return false;
			//return AccountStore.Create().FindAccountsForService(Constants.AppName).Any() ? true : false;
		}
	}
}
