﻿using System;
namespace LavLav
{
	public class Car : BaseModel
	{
		public string description { get; set; }
		public string licensePlate { get; set; }
		public LavlavColor color { get; set; }
		public string createdAt { get; set; }
		public Model model { get; set; }
		public override int GetHashCode(){ return id; }
		public override bool Equals(object obj)
		{
			if (obj == null) return false;
			Car objAsCar = obj as Car;
			if (objAsCar == null) return false;
			else return Equals(objAsCar);
		}
		public bool Equals(Car other)
		{
			if (other == null) return false;
			return (this.id.Equals(other.id));
		}
	}

	public class Manufacturer
	{
		public int id { get; set; }
		public string name { get; set; }
		public string createdAt { get; set; }
	}

	public class Size
	{
		public int id { get; set; }
		public string name { get; set; }
		public string createdAt { get; set; }
	}

	public class Model
	{
		public int id { get; set; }
		public string name { get; set; }
		public string createdAt { get; set; }
		public Manufacturer manufacturer { get; set; }
		public Size size { get; set; }
	}

	public class LavlavColor
	{
		public int id { get; set; }
		public string name { get; set; }
		public string hex { get; set; }
	}
}

