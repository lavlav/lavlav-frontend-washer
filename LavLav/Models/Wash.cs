﻿using System;
using Xamarin.Forms;

namespace LavLav
{
	public class Wash
	{	
		public int Number { get; set; }
		public string Service { get; set; }
		public string Car { get; set; }
		public DateTime Date { get; set; }
		public string Address { get; set; }
		public string StrNumber
		{
			get
			{
				return "#" + Number.ToString();
			}
		}
		public string Description
		{
			get
			{
				return "Serviço: " + Service + '\n' + "Carro: " + Car + '\n' +
					"Data: " + Date.ToString() + '\n' + "Local: " + Address + '\n';
			}
		}
		public string StrDate
		{
			get
			{
				return Date.ToString();
			}
		}
		public Wash(int pNumber, string pService, string pCar, DateTime pDate, string pAddress)
		{
			Number = pNumber;
			Service = pService;
			Car = pCar;
			Date = pDate;
			Address = pAddress;
		}
	}
}

