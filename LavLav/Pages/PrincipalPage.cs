﻿using System;

using Xamarin.Forms;

namespace LavLav
{
	public class PrincipalPage : MasterDetailPage
	{
		MasterPage masterPage;
		DrawerPage drawerPage;

		public PrincipalPage ()
		{
			drawerPage = new DrawerPage();
			Master = drawerPage;
			Detail = new NavigationPage (new WasherPage ()){
				BarTextColor = Color.White,
				BarBackgroundColor = Color.Black
			};

			#region ClickEvents

			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += (sender, e) =>
			{
				this.IsPresented = false;
				Detail.Navigation.PopToRootAsync();
			};
			drawerPage.ListHeader.GestureRecognizers.Add(tapGestureRecognizer);

			drawerPage.SideMenuList.ItemTapped += async (sender, e) =>
			{
				if (e.Item != null)
				{
					this.IsPresented = false;
					var menuModel = (MasterPageItem)e.Item;

					if (menuModel.TargetType != null)
					{
						if (menuModel.TargetType == typeof(OrdersPage))
						{
							var currentPage = ((NavigationPage)Detail).CurrentPage;
							((BasePage)currentPage).IsLoading = true;
							var orders = await WashesController.GetAvailableOrders(LoginController.LoggedUser);
							((BasePage)currentPage).IsLoading = false;
							await Detail.Navigation.PushAsync(new OrdersPage(orders));
						}
						else if (menuModel.TargetType == typeof(AgendaPage))
						{
							var currentPage = ((NavigationPage)Detail).CurrentPage;
							((BasePage)currentPage).IsLoading = true;
							var orders = await WashesController.GetMyOrders();
							((BasePage)currentPage).IsLoading = false;
							await Detail.Navigation.PushAsync(new AgendaPage(orders));
						}
						else
						{
							var page = Activator.CreateInstance(menuModel.TargetType);
							await Detail.Navigation.PushAsync((Page)page);
						}
					}
					else
					{
						await DisplayAlert("Atenção", "Menu disponível nas próximas versões", "OK");
					}

				}
			};

			App.Navigation = Detail.Navigation;
		}
		#endregion
	}
}


