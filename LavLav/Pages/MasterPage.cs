﻿using Xamarin.Forms;
using System.Collections.Generic;

namespace LavLav
{
	public class MasterPage : ContentPage
	{
		public ListView ListView { get { return listView; } }

		public ListView listView;

		public MasterPage ()
		{
			var masterPageItems = new List<MasterPageItem> ();
			masterPageItems.Add (new MasterPageItem {
				Title = LoginController.LoggedUser.name,
				IconSource = "icon.png",
				TargetType = typeof(InfoPage)
			});
			masterPageItems.Add (new MasterPageItem {
				Title = "Agenda",
				IconSource = "icon.png",
				TargetType = typeof(LoginPage)
			});
			masterPageItems.Add (new MasterPageItem {
				Title = "Histórico",
				IconSource = "icon.png",
				TargetType = typeof(InfoPage)
			});
			masterPageItems.Add(new MasterPageItem
			{
				Title = "Desempenho",
				IconSource = "icon.png",
				TargetType = typeof(InfoPage)
			});
			masterPageItems.Add(new MasterPageItem
			{
				Title = "Extrato",
				IconSource = "icon.png",
				TargetType = typeof(InfoPage)
			});
			masterPageItems.Add(new MasterPageItem
			{
				Title = "Preferências",
				IconSource = "icon.png",
				TargetType = typeof(InfoPage)
			});
			masterPageItems.Add(new MasterPageItem
			{
				Title = "Fale Conosco",
				IconSource = "icon.png",
				TargetType = typeof(InfoPage)
			});	
			masterPageItems.Add(new MasterPageItem
			{
				Title = "Indicar Profissional",
				IconSource = "icon.png",
				TargetType = typeof(InfoPage)
			});
			masterPageItems.Add(new MasterPageItem
			{
				Title = "Configurações",
				IconSource = "icon.png",
				TargetType = typeof(InfoPage)
			});
			masterPageItems.Add(new MasterPageItem
			{
				Title = "Compartilhar",
				IconSource = "icon.png",
				TargetType = typeof(InfoPage)
			});

			listView = new ListView {
				ItemsSource = masterPageItems,
				BackgroundColor = Color.Black,
				ItemTemplate = new DataTemplate (() => {
					var imageCell = new ImageCell ();
					imageCell.TextColor = Color.White;
					imageCell.SetBinding (TextCell.TextProperty, "Title");
					imageCell.SetBinding (ImageCell.ImageSourceProperty, "IconSource");
					return imageCell;
				}),
				VerticalOptions = LayoutOptions.FillAndExpand,
				//SeparatorVisibility = SeparatorVisibility.None
			};

			Padding = new Thickness (0, 40, 0, 0);
			Icon = "icon.png";
			Title = "Lav Lav";
			Content = new StackLayout {
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = {
					listView
				}
			};
		}
	}
}


