﻿using System;
using Xamarin.Forms;

namespace LavLav
{
	public class ReducedWashCell : ViewCell
	{
		public ReducedWashCell()
		{
			var order = ((Order)this.BindingContext);
			var frame = new Frame { Margin = 10 };
			var date = new Label
			{
				FontAttributes = FontAttributes.Bold,
				FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label))
			};
			date.SetBinding(Label.TextProperty, "StrStartDate");

			var key = new ColoredInfoLabel("Chave:");
			key.Value.SetBinding(Label.TextProperty, "keyLocation");
			var address = new Label
			{
				TextColor = Color.Gray
			};
			address.SetBinding(Label.TextProperty, "AddressLine1");
			var addressDetails = new Label
			{
				TextColor = Color.Gray
			};
			addressDetails.SetBinding(Label.TextProperty, "AddressLine2");

			var description = new Label
			{
				TextColor = Color.Gray
			};
			description.SetBinding(Label.TextProperty, "Description");


			var buttonStart = new Button
			{
				Text = "Iniciar",
				Margin = 10,
				BackgroundColor = Constants.Colors.LavlavBlue,
				TextColor = Color.White,
				FontAttributes = FontAttributes.Bold
			};
			buttonStart.Clicked += async (sender, e) =>
			{
				var success = await WashesController.StartOrder(order.id);
			};
			var buttonFinish = new Button
			{
				Text = "Cancelar",
				Margin = 10,
				BackgroundColor = Constants.Colors.LavlavRed,
				TextColor = Color.White,
				FontAttributes = FontAttributes.Bold
			};

			var buttonSection = new Grid();
			buttonSection.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
			buttonSection.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
			buttonSection.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
			buttonSection.Children.Add(buttonStart, 0, 0);
			buttonSection.Children.Add(buttonFinish, 1, 0);


			var layout = new StackLayout
			{
				Children = {
					date,
					address,
					addressDetails,
					buttonSection
				}
			};
			frame.Content = layout;
			View = frame;
		}
	}
}
