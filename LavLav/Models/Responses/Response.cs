﻿using System;

using Xamarin.Forms;

namespace LavLav
{
	public class Response 
	{
		public string result { get; set; }
		public string message { get; set; }
	}
}

