﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace LavLav
{
	public partial class LandingPage : ContentPage
	{
		public LandingPage()
		{
			InitializeComponent();
		}

		protected void CheckCrendential()
		{
			Device.BeginInvokeOnMainThread(async () =>
			{
				var credentialsService = DependencyService.Get<ICredentialsService>();
				if (credentialsService.DoCredentialsExist())
				{
					var result = await LoginController.ValidateUser(credentialsService.UserName, credentialsService.Password);
					if (result)
					{
						await Navigation.PushModalAsync(new PrincipalPage());
						return;
					}
				}
				LoginController.ForgotCrendentials();
				Navigation.InsertPageBefore(new LoginPage(), this);
				await Navigation.PopAsync();
			});
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			Task.Run(() => CheckCrendential());
		}
	}
}
