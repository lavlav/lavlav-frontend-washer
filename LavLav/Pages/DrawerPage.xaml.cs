﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace LavLav
{
	public partial class DrawerPage : ContentPage
	{
		public List<MasterPageItem> MenuItens { get; set; }
		public User LogUser { get; set; }
		public ListView SideMenuList { get { return SideMenu; } }
		public StackLayout ListHeader { get { return Header; } }
		public string TESTE
		{
			get
			{
				return "AHHH";
			}
			set { var a = value; }
		}


		public DrawerPage()
		{
			InitializeComponent();

			LogUser = LoginController.LoggedUser;
			MenuItens = new List<MasterPageItem>();

			MenuItens.Add(new MasterPageItem
			{
				Title = "Lavagens",
				IconSource = "carwash.png",
				TargetType = typeof(OrdersPage)
			});
			MenuItens.Add(new MasterPageItem
			{
				Title = "Agenda",
				IconSource = "date.png",
				TargetType = typeof(AgendaPage)
			});
			MenuItens.Add(new MasterPageItem
			{
				Title = "Financeiro",
				IconSource = "money.png"
			});

			SideMenu.ItemsSource = MenuItens;

			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += LogoutClicked;
			LogoutLay.GestureRecognizers.Add(tapGestureRecognizer);

			UserName.Text = LogUser.fullName;
			UserEmail.Text = LogUser.email;
		}

		async void LogoutClicked(object sender, System.EventArgs e)
		{
			LoginController.ForgotCrendentials();
			Application.Current.MainPage = new NavigationPage(new LoginPage());
		}
	}
}
