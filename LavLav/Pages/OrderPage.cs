﻿using System;
using System.Collections;
using System.Collections.Generic;
using ImageCircle.Forms.Plugin.Abstractions;
using Xamarin.Forms;

namespace LavLav
{
	public class OrderPage : BasePage
	{
		Order MyOrder;

		Picker HourPicker;


		public OrderPage(Order pOrder)
		{
			if (pOrder == null)
			{
				DisplayAlert("Erro", "Falha ao carregar dados do pedido", "Ok");
				Navigation.PopAsync();
			}
			Title = "Detalhes do Pedido";
			MyOrder = pOrder;
			LoadLayout(BuildLayout());
		}

		ScrollView BuildLayout()
		{
			var scrollView = new ScrollView();
			TitleFormView addressTitle = new TitleFormView
			{
				Title = "Endereço"
			};
			var addresLine = new Label
			{
				Text = MyOrder.AddressLine1 + '\n' + MyOrder.AddressLine2,
				Margin = 10,
			};
			TitleFormView dateTitle = new TitleFormView
			{
				Title = "Data"
			};
			var dayLabel = new Label
			{
				Margin = new Thickness(10, 10, 10, 0),
				Text = MyOrder.expectedStartBeginAt.ToString("d")
			};
			var hourLabel = new Label
			{
				Margin = new Thickness(10, 0, 10, 10),
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
				Text = MyOrder.expectedStartBeginAt.ToString("t") + "-" + MyOrder.expectedStartEndAt.ToString("t")
			};
			HourPicker = new Picker { HorizontalOptions = LayoutOptions.FillAndExpand };
			loadHourPickerItens();
			var chooseHour = new StackLayout
			{
				Orientation = StackOrientation.Horizontal,
				Padding = 10,
				Children =
				{
					new Label { Text = "Horário Atendimento:", VerticalOptions = LayoutOptions.Center}, HourPicker
				}
			};
			TitleFormView detailsTitle = new TitleFormView
			{
				Title = "Detalhes"
			};
			var key = new Label
			{
				Text = "Chave:",
				FontAttributes = FontAttributes.Bold
			};
			var client = new Label
			{
				Text = "Cliente:",
				FontAttributes = FontAttributes.Bold
			};
			var plug = new Label
			{
				Text = "Tomada:",
				FontAttributes = FontAttributes.Bold
			};
			var keyInfo = new Label
			{
				Text = MyOrder.keyLocation ?? "Info não Disponível"
			};
			var clientInfo = new Label
			{
				Text = MyOrder.user.name ?? "Info não Disponível"
			};
			var plugInfo = new Label
			{
				Text = MyOrder.isPlugAvailable? "Sim" : "Não"
			};
			TitleFormView servicesTitle = new TitleFormView
			{
				Title = "Serviços"
			};

			scrollView.Content= new StackLayout
			{
				VerticalOptions = LayoutOptions.Start,
				Children = {
					addressTitle,
					addresLine,
					dateTitle,
					dayLabel,
					hourLabel,
					chooseHour,
					detailsTitle,
					new StackLayout {
						Orientation = StackOrientation.Horizontal,
						Children = {client, clientInfo},
						Padding = new Thickness(10,0)
					},
					new StackLayout {
						Orientation = StackOrientation.Horizontal,
						Children = {key, keyInfo},
						Padding = new Thickness(10,0)
					},
					new StackLayout {
						Orientation = StackOrientation.Horizontal,
						Children = {plug, plugInfo},
						Padding = new Thickness(10,0)
					},
					servicesTitle,
					buildServiceSection(),
					buildButtonSection()
				}
			};

			return scrollView;
		}

		void loadHourPickerItens()
		{
			if (MyOrder.status.Equals("WA"))
			{
				
				var start = MyOrder.expectedStartBeginAt;
				if (start == MyOrder.expectedStartEndAt)
				{
					HourPicker.Items.Add(start.ToString("t"));
					return;
				}

				var end = MyOrder.expectedStartEndAt.AddMinutes(-MyOrder.Duration());
				List<DateTime> starts = new List<DateTime>();
				while (start < end)
				{
					starts.Add(start);
					HourPicker.Items.Add(start.ToString("t"));
					start = start.AddMinutes(30);
				}
				starts.Add(end);
				HourPicker.Items.Add(end.ToString("t"));
			}
			else
			{
				HourPicker.Items.Add(MyOrder.scheduledAt.ToString("t"));
				HourPicker.SelectedIndex = 0;
				HourPicker.IsEnabled = false;
			}
		}

		StackLayout buildServiceSection()
		{
			var section = new StackLayout();
			var carList = new List<Car>();
			foreach (var item in MyOrder.orderItems)
			{
				if ((item.car != null) && (!carList.Contains(item.car)))
					carList.Add(item.car);
			}

			foreach (var car in carList)
			{
				var carHeader = new StackLayout
				{
					Orientation = StackOrientation.Horizontal,
					HorizontalOptions = LayoutOptions.Center,
					Children = {
						new Label {
							Text = car.model.name,
							VerticalOptions = LayoutOptions.Center
						},
						new CircleImage {
							BorderColor = Color.White,
							BorderThickness = 3,
							HeightRequest = 30,
							WidthRequest = 30,
							Aspect = Aspect.AspectFill,
							FillColor = Color.FromHex(car.color.hex),
							VerticalOptions = LayoutOptions.Center
						}}
				};
				section.Children.Add(carHeader);

				var services = buildServiceList(car);
				var servicesList = new ListView
				{
					ItemsSource = services,
					ItemTemplate = new DataTemplate(typeof(TextCell)),
					HasUnevenRows = true,
					RowHeight = Constants.RowHeight,
					BackgroundColor = Color.Transparent,
					SeparatorVisibility = SeparatorVisibility.None
				};
				servicesList.HeightRequest = Constants.RowHeight * services.Count;

				var listStackLayout = new StackLayout
				{
					VerticalOptions = LayoutOptions.FillAndExpand,
					Orientation = StackOrientation.Vertical
				};
				listStackLayout.Children.Add(servicesList);

				servicesList.ItemTemplate.SetBinding(TextCell.TextProperty, "Title");
				servicesList.ItemTemplate.SetBinding(TextCell.DetailProperty, "Details");
				section.Children.Add(listStackLayout);
			}

			return section;
		}

		View buildButtonSection()
		{
			var buttonStart = new Button
			{
				Text = "Iniciar",
				Margin = 10,
				BackgroundColor = Constants.Colors.LavlavBlue,
				TextColor = Color.White,
				FontAttributes = FontAttributes.Bold
			};

			var buttonFinish = new Button
			{
				Text = "Cancelar",
				Margin = 10,
				BackgroundColor = Constants.Colors.LavlavRed,
				TextColor = Color.White,
				FontAttributes = FontAttributes.Bold
			};

			var buttonSection = new Grid();
			buttonSection.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
			buttonSection.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
			buttonSection.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
			buttonSection.Children.Add(buttonStart, 0, 0);
			buttonSection.Children.Add(buttonFinish, 1, 0);


			switch (MyOrder.status)
			{
				case "CO":
					buttonStart.Clicked += async (sender, e) =>
					{
						IsLoading = true;
						var success = await WashesController.StartOrder(MyOrder.id);
						IsLoading = false;
						if (success)
						{
							await DisplayAlert("Aviso", "Pedido Iniciado", "OK!");
							await Navigation.PopToRootAsync();
						}
						else
						{
							await DisplayAlert("Aviso", "ERROR ao iniciar pedido. Favor tentar novamente", "OK!");
						}
					};

					buttonFinish.Clicked += async (sender, e) =>
					{
						IsLoading = true;
						var success = await WashesController.CancelOrder(MyOrder.id);
						IsLoading = false;
						if (success)
						{
							await DisplayAlert("Aviso", "Pedido Cancelado", "OK!");
							await Navigation.PopToRootAsync();
						}
						else
						{
							await DisplayAlert("Aviso", "ERROR ao cancelar pedido. Favor tentar novamente", "OK!");
						}
					};
					return buttonSection;
				case "ST":
					buttonFinish.Text = "Finalizar Pedido";
					buttonFinish.Clicked += async (sender, e) =>
					{
						IsLoading = true;
						var success = await WashesController.FinishOrder(MyOrder.id);
						IsLoading = false;
						if (success)
						{
							await DisplayAlert("Aviso", "Finalizado", "OK!");
							await Navigation.PopToRootAsync();
						}
						else
						{
							await DisplayAlert("Aviso", "ERROR ao finalizar pedido. Favor tentar novamente", "OK!");
						}
					};
					return buttonFinish;
				case "FI":
				return new Label { Text = "PEDIDO FINALIZADO", 
						HorizontalOptions = LayoutOptions.Center,
						FontAttributes = FontAttributes.Bold};
				case "CA":	
					return new Label { Text = "PEDIDO CANCELADO", 
						HorizontalOptions = LayoutOptions.Center,
						FontAttributes = FontAttributes.Bold };
				default:
					buttonStart.Text = "Aceitar Pedido";
					buttonStart.Clicked += async (sender, e) =>
					{
						if (HourPicker.SelectedIndex != -1)
						{
							IsLoading = true;
							var response = await WashesController.ReserveOrder(MyOrder.id, (System.DateTime)getSelectedDate());
							IsLoading = false;
							if (!response.result.Equals(Constants.Error))
							{
								await DisplayAlert("Aviso", "Pedido Aceito", "OK!");
								await Navigation.PopToRootAsync();
							}
							else
							{
								await DisplayAlert("Erro ao Reservar", response.message, "OK!");
							}
						}
						else
						{
							await DisplayAlert("Aviso", "Você deve selecionar um horário de atendimento", "OK!");
							HourPicker.Focus();
						}
					};
					return buttonStart;
			}
		}

		DateTime? getSelectedDate()
		{
			if (HourPicker == null) return null;
			if (HourPicker.SelectedIndex == -1) return null;

			var hour = DateTime.Parse(HourPicker.Items[HourPicker.SelectedIndex]);
			return new DateTime(MyOrder.expectedStartBeginAt.Year,
								MyOrder.expectedStartBeginAt.Month,
								MyOrder.expectedStartBeginAt.Day,
								hour.Hour, hour.Minute, hour.Second);
		}

		List<TitleDetails> buildServiceList(Car pCar)
		{
			List<TitleDetails> serviceList = new List<TitleDetails>();
			var filteredItems = MyOrder.orderItems.FindAll((obj) => obj.car.Equals(pCar));
			foreach (var item in filteredItems)
			{
				var serviceName = item.service != null ? item.service.name : item.addon.name;
				serviceList.Add(new TitleDetails { Title = serviceName, Details = "R$"+item.price.amount });
			}
			return serviceList;
		}
	}

	class TitleDetails
	{
		public string Title { get; set;}
		public string Details { get; set;}
	}
}
