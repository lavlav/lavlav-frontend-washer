﻿using System;

using Xamarin.Forms;

namespace LavLav
{
	public class LoginPage : BasePage
	{
		Entry emailEntry;
		Entry passwordEntry;
		Button loginButton;
		Label loginFeedback;

		public LoginPage ()
		{
			NavigationPage.SetHasNavigationBar(this, false);

			emailEntry = new Entry {
				Placeholder = "Email"
			};
			passwordEntry = new Entry
			{
				Placeholder = "Senha",
				IsPassword = true
			};

			loginFeedback = new Label
			{
				TextColor = Constants.Colors.LavlavRed
			};

			loginButton = new Button {
				Text = "Entrar",
				BackgroundColor = Color.Black,
				TextColor = Color.White,
				FontAttributes = FontAttributes.Bold
			};

			var registerLabell = new Label
			{
				Text = "Registrar",
				TextColor = Constants.Colors.LavlavBlue,
				FontAttributes = FontAttributes.Bold,
				HorizontalOptions = LayoutOptions.Center,
				Margin = new Thickness(10, 20, 10, 10)
			};

			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += OnRegisterClicked;
			registerLabell.GestureRecognizers.Add(tapGestureRecognizer);

			loginButton.Clicked += onLogin;

			var layout = new StackLayout
			{
				VerticalOptions = LayoutOptions.Center,
				BackgroundColor = Color.Transparent,
				Children = {

					new StackLayout {
						Padding = new Thickness (10, 10, 10, 10),
						Children = {
							new Label {
								Text = "BEM VINDO",
								HorizontalOptions = LayoutOptions.Center,
								FontAttributes = FontAttributes.Bold,
								FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
							},
							new Image {
								Source = "logo.png",
								Scale = 0.8
							},
							emailEntry,
							passwordEntry,
							loginFeedback,
							loginButton
						}
					},

					//new StackLayout {
					//	Orientation = StackOrientation.Horizontal,
					//	Children = {
					//		new BoxView {
					//			Color = Color.Black,
					//			HeightRequest = 1,
					//			VerticalOptions = LayoutOptions.Center,
					//			HorizontalOptions = LayoutOptions.FillAndExpand
					//		},
					//		new Label {
					//			Text = " Ou "
					//		},
					//		new BoxView {
					//			Color = Color.Black,
					//			HeightRequest = 1,
					//			VerticalOptions = LayoutOptions.Center,
					//			HorizontalOptions = LayoutOptions.FillAndExpand
					//		}
					//	}
					//},

					//registerLabell
				}
			};

			LoadLayout(new ScrollView { Content = layout, BackgroundColor = Color.Transparent });
		}

		async void OnRegisterClicked(object sender, EventArgs e)
		{
			//await Navigation.PushAsync(new RegisterPage());
		}

		async void onLogin (object sender, EventArgs e)
		{
			IsLoading = true;
			loginFeedback.Text = "";
			var result = await LoginController.ValidateUser(emailEntry.Text, passwordEntry.Text);
			if (result)
			{
				SuccessfulLogin();
			}
			else {
				loginFeedback.Text = "Login inválido!";
			}
			IsLoading = false;
		}

		public void SuccessfulLogin()
		{
			Navigation.PushModalAsync(new PrincipalPage());
		}
	}
}


