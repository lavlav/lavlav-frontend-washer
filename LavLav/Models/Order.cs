﻿using System;
using System.Collections.Generic;

namespace LavLav
{
	/* 🔥 A Classe de Negócio Principal 🔥 */
	public class Order : BaseModel
	{
		public DateTime expectedStartBeginAt { get; set; }
		public DateTime expectedStartEndAt { get; set; }
		public DateTime acceptedAt { get; set; }
		public DateTime scheduledAt { get; set; }
		public DateTime createdAt { get; set; }
		public bool isPlugAvailable { get; set; }
		public String keyLocation { get; set; }
		public String comments { get; set; }
		public User user { get; set; }
		public Address address { get; set; }
		public String status { get; set; }
		public List<OrderItem> orderItems { get; set; }

		public String StrId
		{
			get
			{
				return "#" + id;
			}
		}

		public String StrCar
		{
			get
			{
				String strCar = "";
				foreach (var oItem in orderItems)
				{
					if (!strCar.Contains(oItem.car.model.name))
				    	strCar = strCar + "/" + oItem.car.model.name;
				}
				return strCar.Substring(1, strCar.Length - 1);
			}
		}

		public String StrStartDate
		{
			get
			{
				return expectedStartBeginAt.ToString("dd/MM/yyyy HH:mm")+expectedStartEndAt.ToString(" - HH:mm") ;
			}
		}

		public String StrStartDay
		{
			get
			{
				return expectedStartBeginAt.ToString("d");
			}
		}

		public String StrStartHour
		{
			get
			{
				return expectedStartBeginAt.ToString("t") + "-" + expectedStartEndAt.ToString("t");
			}
		}

		public String StrDaysToExecute
		{
			get
			{
				var days =  (expectedStartBeginAt - DateTime.Today).Days;
				if (days < 0)
				{
					return "Pedido Atrasado.";
				}
				if (days <= 1)
				{
					if (expectedStartBeginAt.Day == DateTime.Today.Day)
					{
						return "Hoje!";
					}
					else
					{
						return "Amanha";
					}
				}
				return days + " dias restantes.";
			}
		}

		public String StrService
		{
			get
			{
				String strService = "";
				foreach (var oItem in orderItems)
				{
					if (oItem.service != null)
						strService = strService + "/" + oItem.service.name;
				}
				return strService.Substring(1,strService.Length-1);
			}
		}

		public String AddressLine1
		{
			get
			{
				return address.city.name + " - " + address.neighborhood;
			}
		}

		public String AddressLine2
		{
			get
			{
				return address.streetName + ", " + address.streetNumber;
			}
		}

		internal int Duration()
		{
			int totalDuration = 0;
			foreach (var item in orderItems)
			{
				if (item.service != null)
					totalDuration+=item.service.duration;
			}
			return totalDuration;
		}
	}

	public class Price : BaseModel
	{
		public string createdAt { get; set; }
		public string amount { get; set; }
		public bool isPromotional { get; set; }
		public string validFrom { get; set; }
		public object validTo { get; set; }
	}

	public class OrderItem : BaseModel
	{
		public Service service { get; set; }
		public Addon addon { get; set; }
		public Price price { get; set; }
		public Car car { get; set; }
	}
}
