﻿
using Android.App;
using Android.Content.PM;
using Android.OS;
using Gcm.Client;
using ImageCircle.Forms.Plugin.Droid;
using Java.Lang;

namespace LavLav.Droid
{
	[Activity(Label = "LavLav", Icon = "@drawable/icon", Theme = "@style/MyTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
	{
		public static MainActivity instance;

		protected override void OnCreate(Bundle bundle)
		{
			instance = this;

			TabLayoutResource = Resource.Layout.Tabbar;
			ToolbarResource = Resource.Layout.Toolbar;

			base.OnCreate(bundle);

			global::Xamarin.Forms.Forms.Init(this, bundle);
			ImageCircleRenderer.Init();

			LoadApplication(new App());

			RegisterWithGCM();
		}

		private void RegisterWithGCM()
		{
			try
			{
				// Check to ensure everything's set up right
				GcmClient.CheckDevice(this);
				GcmClient.CheckManifest(this);

				// Register for push notifications
				System.Diagnostics.Debug.WriteLine("Registrando...");
				GcmClient.Register(this, Constants.SenderID);
			}
			catch (Java.Net.MalformedURLException)
			{
				CreateAndShowDialog((Java.Lang.String)"There was an error creating the client. Verify the URL.", (Java.Lang.String)"Error");
			}
			catch (Exception e)
			{
				CreateAndShowDialog((Java.Lang.String)e.Message, (Java.Lang.String)"Error");
			}
		}

		private void CreateAndShowDialog(String message, String title)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);

			builder.SetMessage(message);
			builder.SetTitle(title);
			builder.Create().Show();
		}
	}
}
