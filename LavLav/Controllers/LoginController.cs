﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace LavLav
{
	public static class LoginController
	{
		static string _Token;
		static public User LoggedUser { set; get; }

		public static string Token {
			get { return _Token; }
		}

		public static void SaveToken(string pToken)
		{
			_Token = pToken;
		}

		public static bool IsLoggedIn {
			get { return !string.IsNullOrWhiteSpace(_Token); }
		}

		public async static Task<bool> ValidateUser(string pUserName, string pUserPass)
		{
			var token = await App.RequestHandler.GetLoginToken(pUserName, pUserPass);
			var success = token.access_token != null;

			if (success)
			{
				_Token = token.access_token;
				App.RequestHandler.SetClient(_Token);
				var user = await App.RequestHandler.GetUser();
				success = user != null;
				if (!success) return false;
				LoggedUser = user;
				LoggedUser.imageURL = "https://maxcdn.icons8.com/Color/PNG/512/Users/circled_user_male-512.png";//"http://static2.blastingnews.com/media/photogallery/2016/4/2/290x290/b_290x290/michel-temer-abandona-lideranca-do-pmdb_664285.jpg";
				SaveCredentialsLocal(pUserName,pUserPass);
			}

			return success;
		}

		static void SaveCredentialsLocal(string pUserName, string pUserPass)
		{
			var storeService = DependencyService.Get<ICredentialsService>();
			bool doCredentialsExist = storeService.DoCredentialsExist();
			if (!doCredentialsExist)
			{
				storeService.SaveCredentials(pUserName, pUserPass);
			}
		}

		public static void ForgotCrendentials()
		{
			DependencyService.Get<ICredentialsService>().DeleteCredentials();
		}
	}
}

