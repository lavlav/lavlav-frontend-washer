﻿using System;

using Xamarin.Forms;

namespace LavLav
{
	public abstract class BasePage : ContentPage
	{
		ActivityIndicator LoadingIndicator;
		bool _IsLoading;
		AbsoluteLayout Overlay;
		Grid BaseLayout;

		public bool IsLoading
		{
			get
			{
				return _IsLoading;
			}

			set
			{
				Overlay.IsVisible = value;
				LoadingIndicator.IsRunning = value;
				_IsLoading = value;
			}
		}

		protected BasePage()
		{
			BaseLayout = new Grid();
			BaseLayout.RowDefinitions.Add(new RowDefinition { 
				Height = new GridLength(1, GridUnitType.Star) });
			
			Overlay = new AbsoluteLayout
			{

				BackgroundColor = Constants.Colors.BackgroundOverlay,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			LoadingIndicator = new ActivityIndicator();

			AbsoluteLayout.SetLayoutFlags(Overlay, AbsoluteLayoutFlags.All);
			AbsoluteLayout.SetLayoutBounds(Overlay, new Rectangle(0, 0, 1, 1));

			AbsoluteLayout.SetLayoutFlags(LoadingIndicator, AbsoluteLayoutFlags.PositionProportional);
			AbsoluteLayout.SetLayoutBounds(LoadingIndicator, new Rectangle(0.5, 0.5, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize));

			Overlay.Children.Add(LoadingIndicator);

			LoadingIndicator.Color = Constants.Colors.LavlavRed;
			LoadingIndicator.Scale = 03;
			Overlay.IsVisible = false;

		}

		protected void LoadLayout(Layout externalContent)
		{
			BaseLayout.Children.Clear();
			AbsoluteLayout.SetLayoutFlags(externalContent, AbsoluteLayoutFlags.All);
			AbsoluteLayout.SetLayoutBounds(externalContent, new Rectangle(0, 0, 1, 1));

			externalContent.BackgroundColor = Color.Transparent;

			var background = new Image { Source = "b.png",
				VerticalOptions = LayoutOptions.FillAndExpand,
				Aspect = Aspect.AspectFill         
			};

			BaseLayout.Children.Add(background);
			BaseLayout.Children.Add(externalContent);
			BaseLayout.Children.Add(Overlay);
			Content = BaseLayout;
		}

		protected double GetWidth() { return BaseLayout.Width; }

	}
}

