﻿using System;
using Xamarin.Forms;

namespace LavLav
{
	public class ColoredInfoLabel : ContentView
	{
		public Label Description;
		public Label Value;
		public ColoredInfoLabel()
		{
			Description = new Label
			{
				TextColor = Constants.Colors.LavlavBlue,
				FontAttributes = FontAttributes.Bold,
				HorizontalTextAlignment = TextAlignment.Start
			};
			Value = new Label
			{
				TextColor = Color.Gray,
				HorizontalTextAlignment = TextAlignment.End,
			};

			Content = new StackLayout
			{
				Orientation = StackOrientation.Horizontal,
				Children = {
					Description, Value
				},

			};
		}

		public ColoredInfoLabel(string pDescription) : this()
		{
			Description.Text = pDescription;
		}
	}
}
