﻿using System;
using Xamarin.Forms;

namespace LavLav
{
	public static class Constants
	{
		public const string AppName = "com.chagas.lavlav";
		public const string SenderID = "992279061162"; // Google API Project Number
		public const string ListenConnectionString = "Endpoint=sb://lavlavnamespace.servicebus.windows.net/;SharedAccessKeyName=DefaultListenSharedAccessSignature;SharedAccessKey=377fTjYGVJ5ih4zSlx5c2DMlnnyc4weBajjHAnh2z7E=";
		public const string NotificationHubName = "lavlavNotificationHub";

		public const string ClientID = "503939556406599";
		public const string FacebookAuthorizeUrl = "https://m.facebook.com/dialog/oauth/";
		public const string FacebookRedirectUrl = "http://m.facebook.com/connect/login_success.html";
		public const double DefaultSpacing = 10;

		public const string BaseAddress = "https://api.lavlav.com.br/";//"http://lavlav-backend-bsaliba92277.codeanyapp.com/";
		public const string RestClientId = "lavlav";
		public const string RestClientSecret = "k4aC2e572WME0h221A1aCAZ9V958YGWI" ;
 		public const string RestUsername = "facebook" ;
		public const string RestGrantType = "password" ;
		internal static readonly int RowHeight = 40;

		public static string Error = "error";

		public static class Colors
		{
			public static readonly Color LavlavBlue = Color.FromHex("00BBDF");
			public static readonly Color LavlavRed = Color.FromHex("FF6371");
			public static readonly Color GerdauOrange = Color.FromHex("FEC82F");
			public static readonly Color GerdauYellow = Color.FromHex("EFDE1D");
			public static readonly Color GerdauBlueDisable = Color.FromHex("337ab7");
			public static readonly Color BackgroundOverlay = Color.FromRgba(94, 128, 189, 162); 	
			public static readonly Color BackgroundToolbar = Color.FromHex("117fc1");
			public static readonly Color HeaderColor = Color.FromRgba(242, 242, 242, 162);
			public static readonly Color TextTitle = Color.FromHex("808080");
			public static readonly Color BackgroudTitle = Color.FromHex("f2f2f2");
		}
	}
}

