﻿using System;
namespace LavLav
{
	public class BaseModel
	{
		public int id { get; set; }
		public override int GetHashCode() { return id; }
		public override bool Equals(object obj)
		{
			if (obj == null) return false;
			BaseModel objAsCar = obj as BaseModel;
			if (objAsCar == null) return false;
			else return Equals(objAsCar);
		}
		public bool Equals(BaseModel other)
		{
			if (other == null) return false;
			return (this.id.Equals(other.id));
		}
	}
}
