﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LavLav
{
	public interface IRestService
	{
		Task<LoginToken> GetLoginToken(String pUsername, String pPassword);
		Task<User> GetUser();
		Task<List<Order>> GetAvailableOrders();
		Task<List<Order>> GetMyOrders();
		Task<Response> ReserveOrder(int pOrderID, DateTime pScheduledAt);
		Task<Response> StartOrder(int pOrderID);
		Task<bool> FinishOrder(int pOrderID);
		Task<Order> GetOrder(int pOrderID);
		void SetClient(String pToken);
	}
}
