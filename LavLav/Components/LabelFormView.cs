﻿using System;

using Xamarin.Forms;

namespace LavLav
{
	public class LabelFormView : ContentView
	{
		public LabelFormView(string pLabel, string pText)
		{
			Label label = new Label
			{
				Text = pLabel
			};

			Label text = new Label
			{
				Text = pText
			};
			
			Content = new StackLayout { 
				Orientation = StackOrientation.Horizontal,
				Children = {
					label, text
				} 
			};
		}
	}
}


