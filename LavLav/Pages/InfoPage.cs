﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace LavLav
{
	public class InfoPage : ContentPage
	{
		ListView washesViewList = new ListView();
		List<Wash> washesList; 

		public InfoPage()
		{
			PopulateWashesList();
			Padding = new Thickness(10, 10, 10, 10);
			Content = new StackLayout
			{
				Children = {
					new Label {
						Text= "Aguardando aceitação",
						HorizontalOptions = LayoutOptions.Center
					},
					washesViewList
				}
			};
		}

		void PopulateWashesList()
		{
			washesList = WashesController.GetWaitingWashesList();

			washesViewList = new ListView
			{
				RowHeight = 180,
				ItemsSource = washesList,
				ItemTemplate = new DataTemplate(typeof(WashCell_old)),
				VerticalOptions = LayoutOptions.FillAndExpand
				//SeparatorVisibility = SeparatorVisibility.None
			};

			washesViewList.ItemTapped += async (sender, e) =>
			{
				if (e.Item != null)
				{

				}
			};
		}

	}
}


