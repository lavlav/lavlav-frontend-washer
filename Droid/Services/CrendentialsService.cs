﻿using System.Linq;
using LavLav.Droid;
using Xamarin.Auth;
using Xamarin.Forms;

[assembly: Dependency(typeof(CredentialsService))]
namespace LavLav.Droid
{
	public class CredentialsService : ICredentialsService
	{
		public string UserName
		{
			get
			{
				var account = AccountStore.Create(Forms.Context).FindAccountsForService(Constants.AppName).FirstOrDefault();
				return (account != null) ? account.Username : null;
			}
		}

		public string Password
		{
			get
			{
				var account = AccountStore.Create(Forms.Context).FindAccountsForService(Constants.AppName).FirstOrDefault();
				return (account != null) ? account.Properties["Password"] : null;
			}
		}

		public void SaveCredentials(string userName, string password)
		{
			if (!string.IsNullOrWhiteSpace(userName) && !string.IsNullOrWhiteSpace(password))
			{
				Account account = new Account
				{
					Username = userName
				};
				account.Properties.Add("Password", password);
				AccountStore.Create(Forms.Context).Save(account, Constants.AppName);
			}
		}

		public void DeleteCredentials()
		{
			var account = AccountStore.Create(Forms.Context).FindAccountsForService(Constants.AppName).FirstOrDefault();
			if (account != null)
			{
				AccountStore.Create(Forms.Context).Delete(account, Constants.AppName);
			}
		}


		public bool DoCredentialsExist()
		{
			return AccountStore.Create(Forms.Context).FindAccountsForService(Constants.AppName).Any() ? true : false;
		}
	}
}
