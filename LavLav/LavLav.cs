﻿using System;

using Xamarin.Forms;

namespace LavLav
{
	public class App : Application
	{
		public static RestService RequestHandler { get; private set; }
		public static INavigation Navigation { get; set; }
		public App()
		{
			// WebService Client Instance
			RequestHandler = new RestService();
			MainPage = new NavigationPage(new LandingPage());
			Navigation = MainPage.Navigation;
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
			var credentialsService = DependencyService.Get<ICredentialsService>();
			if (!(credentialsService.DoCredentialsExist() && LoginController.Token != null))
			{
				LoginController.ForgotCrendentials();
				MainPage = new NavigationPage(new LoginPage());
			}
		}
	}
}
