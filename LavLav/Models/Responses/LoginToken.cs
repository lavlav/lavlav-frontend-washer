﻿using System;
namespace LavLav
{
	public class LoginToken
	{
		public string id_token { get; set; }
		public string access_token { get; set; }
		public string token_type { get; set; }
	}
}

