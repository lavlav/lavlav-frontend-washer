﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace LavLav
{
	public class AgendaPage : BasePage
	{
		public List<Order> Orders { get; private set; }

		public AgendaPage(List<Order> pOrders)
		{
			Title = "Agenda";
			Orders = pOrders;
			LoadLayout(BuildLayout());
		}

		Layout BuildLayout()
		{
			var listView = new ListView
			{
				ItemsSource = Orders,
				ItemTemplate = new DataTemplate(typeof(ReducedOrderCell)),
				VerticalOptions = LayoutOptions.FillAndExpand,
				SeparatorVisibility = SeparatorVisibility.None,
				HasUnevenRows = true, //RowHeight = 250,
				BackgroundColor = Color.Transparent
			};

			listView.ItemTapped += async (sender, e) =>
			{
				Order item = (Order)e.Item;
				if (item != null)
				{
					IsLoading = true;
					var order = await WashesController.GetOrder(item.id);
					IsLoading = false;
					Navigation.PushAsync(new OrderPage(order));
				}
			};

			return new StackLayout
			{
				Padding = 10,
				Children = {
					listView
				}
			};
		}
	}
}

