﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace LavLav
{
	public class RestService : IRestService
	{
		HttpClient Client;

		public void SetClient(String pToken)
		{
			var tokenString = string.Format("{0} {1}", "Bearer", pToken);

			Client = new HttpClient();
			Client.BaseAddress = new Uri(Constants.BaseAddress);
			Client.DefaultRequestHeaders.Add("Authorization", tokenString);
			Client.DefaultRequestHeaders.Add("Accept", "application/json");
		}

		public async Task<User> GetUser()
		{
			var uri = new Uri(string.Format("/api/v1/user/get-info"));
			try
			{
				var response = await Client.GetAsync(uri);

				if (response.IsSuccessStatusCode)
				{
					var responceContent = await response.Content.ReadAsStringAsync();
					var userResponse = JsonConvert.DeserializeObject<User>(responceContent);
					Debug.WriteLine(@"				UserResponse Success.");
					return userResponse;
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(@"				ERROR {0}", ex.Message);
			}
			return null;
		}

		public async Task<List<Order>> GetAvailableOrders()
		{
			var uri = new Uri(string.Format("/api/v1/available-orders"));
			try
			{
				var response = await Client.GetAsync(uri);

				if (response.IsSuccessStatusCode)
				{
					var responceContent = await response.Content.ReadAsStringAsync();
					var ordersResponse = JsonConvert.DeserializeObject<List<Order>>(responceContent);
					Debug.WriteLine(@"				OrdersResponse Success.");
					return ordersResponse;
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(@"				ERROR {0}", ex.Message);
			}
			return null;
		}

		public async Task<List<Order>> GetMyOrders()
		{
			var uri = new Uri(string.Format("/api/v1/my-orders"));
			try
			{
				var response = await Client.GetAsync(uri);

				if (response.IsSuccessStatusCode)
				{
					var responceContent = await response.Content.ReadAsStringAsync();
					var ordersResponse = JsonConvert.DeserializeObject<List<Order>>(responceContent);
					Debug.WriteLine(@"				MyOrdersResponse Success.");
					return ordersResponse;
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(@"				ERROR {0}", ex.Message);
			}
			return null;
		}

		public async Task<Response> ReserveOrder(int pOrderID, DateTime pScheduledAt)
		{
			DateTimeOffset scheduledAtOffset = new DateTimeOffset(pScheduledAt);
			var uri = new Uri(string.Format("/api/v1/order/"+pOrderID+"/reserve"));
			try
			{
				var jsonRequest = JsonConvert.SerializeObject(new
				{
					scheduledAt = scheduledAtOffset
				});

				var content = new StringContent(jsonRequest.ToString(), Encoding.UTF8, "application/json");
				content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
				var response = await Client.PostAsync(uri,content);

				if (response.IsSuccessStatusCode)
				{
					var responceContent = await response.Content.ReadAsStringAsync();
					var responseObject = JsonConvert.DeserializeObject<Response>(responceContent);
					Debug.WriteLine(@"				ReserveOrder Success.");
					return responseObject;
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(@"				ERROR {0}", ex.Message);
				return new Response { message = "Erro ao iniciar pedido, tente mais tarde", result = "error" };
			}
			return new Response { message = "Erro ao iniciar pedido, tente mais tarde", result = "error" };
		}

		public async Task<Response> StartOrder(int pOrderID)
		{
			var uri = new Uri(string.Format("/api/v1/order/" + pOrderID + "/start"));
			try
			{
				
				var response = await Client.PostAsync(uri, null);

				if (response.IsSuccessStatusCode)
				{
					var responceContent = await response.Content.ReadAsStringAsync();
					var responseObject = JsonConvert.DeserializeObject<Response>(responceContent);
					Debug.WriteLine(@"				StartOrder Success.");
					return responseObject;
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(@"				ERROR {0}", ex.Message);
				return new Response { message = "Erro ao iniciar pedido, tente mais tarde", result = "error" };
			}

			return new Response { message = "Erro ao iniciar pedido, tente mais tarde", result = "error" };
		}

		public async Task<bool> FinishOrder(int pOrderID)
		{
			var uri = new Uri(string.Format("/api/v1/order/" + pOrderID + "/finish"));
			try
			{

				var response = await Client.PostAsync(uri, null);

				if (response.IsSuccessStatusCode)
				{
					Debug.WriteLine(@"				FinishOrder Success.");
					return true;
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(@"				ERROR {0}", ex.Message);
				return false;
			}
			return false;
		}

		public async Task<bool> CancelOrder(int pOrderID)
		{
			var uri = new Uri(string.Format("/api/v1/order/" + pOrderID + "/cancel"));
			try
			{

				var response = await Client.PostAsync(uri, null);

				if (response.IsSuccessStatusCode)
				{
					Debug.WriteLine(@"				CancelOrder Success.");
					return true;
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(@"				ERROR {0}", ex.Message);
				return false;
			}
			return false;
		}

		public async Task<Order> GetOrder(int pOrderID)
		{
			var uri = new Uri(string.Format("/api/v1/order/" + pOrderID ));
			try
			{
				var response = await Client.GetAsync(uri);

				if (response.IsSuccessStatusCode)
				{
					Debug.WriteLine(@"				GetOrder Success.");
					var responceContent = await response.Content.ReadAsStringAsync();
					var orderResponse = JsonConvert.DeserializeObject<Order>(responceContent);
					return orderResponse;
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(@"				ERROR {0}", ex.Message);
			}
			return null;
		}

		public async Task<LoginToken> GetLoginToken(String pUsername, String pPassword)
		{
			var client = new HttpClient();
			client.BaseAddress = new Uri(Constants.BaseAddress);
			//client.MaxResponseContentBufferSize = 256000;

			var token = new LoginToken();
			var uri = new Uri(string.Format("/oauth"));

			try
			{
				var content = new FormUrlEncodedContent(new[]
				{
					new KeyValuePair<string, string>("grant_type", Constants.RestGrantType),
					new KeyValuePair<string, string>("client_id", Constants.RestClientId),
					new KeyValuePair<string, string>("client_secret", Constants.RestClientSecret),
					new KeyValuePair<string, string>("username", pUsername),
					new KeyValuePair<string, string>("password", pPassword)
				});

				HttpResponseMessage response = null;

				response = await client.PostAsync(uri, content);

				if (response.IsSuccessStatusCode)
				{
					var responceContent = await response.Content.ReadAsStringAsync();
					token = JsonConvert.DeserializeObject<LoginToken>(responceContent);
					Debug.WriteLine(@"				GetLoginToken Success.");

				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(@"				ERROR {0}", ex.Message);
			}

			return token;
		}
	}
}
