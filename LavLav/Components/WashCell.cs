﻿using System;

using Xamarin.Forms;

namespace LavLav
{
	public class WashCell : ViewCell
	{
		public WashCell()
		{
			var frame = new Frame { Margin = 10 };

			var date = new Label();
			date.SetBinding(Label.TextProperty, "StrStartDate");

			var numOrder = new Label
			{
				FontAttributes = FontAttributes.Bold
			};
			numOrder.SetBinding(Label.TextProperty, "StrId");

			var daysToExecute = new Label { 
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
				HorizontalTextAlignment = TextAlignment.End
			};
			daysToExecute.SetBinding(Label.TextProperty, "StrDaysToExecute");

			var headerGrid = new Grid();
			headerGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
			headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
			headerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
			headerGrid.Children.Add(numOrder, 0, 0);
			headerGrid.Children.Add(daysToExecute, 1, 0);

			var car = new ColoredInfoLabel("Carro:");
			car.Value.SetBinding(Label.TextProperty, "StrCar");
			var key = new ColoredInfoLabel("Chave:");
			key.Value.SetBinding(Label.TextProperty, "keyLocation");
			var address = new ColoredInfoLabel("Local:");
			address.Value.SetBinding(Label.TextProperty, "AddressLine1");
			var addressDetails = new Label
			{
				TextColor = Color.Gray
			};
			addressDetails.SetBinding(Label.TextProperty, "AddressLine2");


			var description = new Label
			{
				TextColor = Color.Gray
			};
			description.SetBinding(Label.TextProperty, "Description");


			var layout = new StackLayout
			{
				Children = {
					headerGrid,
					date,
					car,
					address,
					addressDetails
				}
			};
			frame.Content = layout;
			View = frame;
		}
	}
}

