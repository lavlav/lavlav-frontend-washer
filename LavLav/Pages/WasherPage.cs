﻿using System;
using ImageCircle.Forms.Plugin.Abstractions;
using Xamarin.Forms;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace LavLav
{
	public class WasherPage : BasePage
	{
		User Washer;
		public Grid RadarGrid { get; private set; }
		public StackLayout RadarLoading { get; private set; }
		public StackLayout RadarFound { get; private set; }
		public StackLayout RadarEmpty { get; private set; }
		public List<Order> Orders { get; private set; }
		public Label AddressLabel { get; private set; }
		public Label ServiceLabel { get; private set; }



		public WasherPage()
		{
			Washer = LoginController.LoggedUser;
			Title = "Dashboard";
			LoadLayout(BuildLayout());
			AddRefreshButton();
			Refresh();

			/*CircleImage photo = new CircleImage
			{
				BorderColor = Color.White,
				BorderThickness = 3,
				HeightRequest = 150,
				WidthRequest = 150,
				Aspect = Aspect.AspectFill,
				HorizontalOptions = LayoutOptions.Center,
				Source = ImageSource.FromUri(new Uri(wahser.ImageURL))
			};
			var rating = new RatingStars();
			rating.SetValue(RatingStars.RatingProperty, wahser.Rating);
			rating.SetValue(RatingStars.ReviewsProperty, 54);
			rating.HorizontalOptions = LayoutOptions.Center;

			Label name = new Label
			{
				Text = wahser.Name,
				HorizontalOptions = LayoutOptions.Center
			};
			Content = new StackLayout
			{
				Children = {
					photo,
					name,
					rating
				}
			};*/
		}

		void AddRefreshButton()
		{
			ToolbarItems.Add(new ToolbarItem("Atualizar", "refreshWhite.png", () =>
			{
				Refresh();
			}));
		}

		async void Refresh()
		{
			UpdateInfoFrame(0);
			Orders = await WashesController.GetAvailableOrders(LoginController.LoggedUser);
			if (Orders != null && Orders.Count > 0)
			{
				populateRadarFound();
				UpdateInfoFrame(1);
			}
			else
			{
				UpdateInfoFrame(2);
			}
		}

		void UpdateInfoFrame(int pFrameTab)
		{
			switch (pFrameTab)
			{
				// 0 - Loading
				case 0:
					RadarLoading.IsVisible = true;
					RadarFound.IsVisible = false;
					RadarEmpty.IsVisible = false;
					break;
				// 1 - We have washes :)
				case 1:
					RadarFound.IsVisible = true;
					RadarLoading.IsVisible = false;
					RadarEmpty.IsVisible = false;
					break;
				// 2 - No donuts for you :(	
				case 2:
					RadarEmpty.IsVisible = true;
					RadarLoading.IsVisible = false;
					RadarFound.IsVisible = false;
					break;
			}
		}

		Layout BuildLayout()
		{
			CircleImage photo = new CircleImage
			{
				BorderColor = Color.White,
				BorderThickness = 3,
				HeightRequest = 100,
				WidthRequest = 100,
				Aspect = Aspect.AspectFill,
				HorizontalOptions = LayoutOptions.Start,
				Source = ImageSource.FromUri(new Uri(Washer.imageURL))
			};

			var name = new Label
			{
				Text = Washer.name,
				FontAttributes = FontAttributes.Bold,
				FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label))
			};
			var email = new Label
			{
				Text = Washer.email,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				TextColor = Color.Gray
			};
			var score = new Label
			{
				Text = "Média: -", //+ Washer.rating,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				TextColor = Color.Gray
			};

			var header = new StackLayout
			{
				Padding = 5,
				BackgroundColor = Constants.Colors.HeaderColor,
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = {
					photo,
					new StackLayout {
						Children = {name,email,score}
					}
				}
			};

			var radarLabel = new Label
			{
				Text = "Sem Pedidos na sua Área",
				FontAttributes = FontAttributes.Bold,
				HorizontalOptions = LayoutOptions.Center
			};

			RadarGrid = new Grid();
			RadarGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
			RadarEmpty = new StackLayout
			{
				Children = 
				{
					new Image { Source = "carwash.png"},
					radarLabel
				},
				HorizontalOptions = LayoutOptions.CenterAndExpand
			};
			var loadingLabel = new Label
			{
				Text = "Carregando Pedidos...",
				FontAttributes = FontAttributes.Bold,
				HorizontalOptions = LayoutOptions.Center
			};
			RadarLoading = new StackLayout
			{
				Children = 
				{
					new Image { Source = "refresh.png"},
					loadingLabel
				},
				HorizontalOptions = LayoutOptions.CenterAndExpand
			};
			RadarLoading.IsVisible = false;

			#region FoundRadar
			var headerFound = new StackLayout
			{
				Orientation = StackOrientation.Horizontal,
				Children = {
					new Image { Source = "carwash.png", HeightRequest = 50},
					new Label
					{
						Text = "Pedido encontrado",
						FontAttributes = FontAttributes.Bold,
						HorizontalOptions = LayoutOptions.Center,
						VerticalTextAlignment = TextAlignment.Center
					},
					new Image { Source = "carwash.png", HeightRequest = 50},
				},
				HorizontalOptions = LayoutOptions.Center
			};
			AddressLabel = new Label
			{
				Text = "asd",
				FontAttributes = FontAttributes.Bold,
				HorizontalOptions = LayoutOptions.Start
			};
			ServiceLabel = new Label
			{
				Text = "asd",
				HorizontalOptions = LayoutOptions.Start
			};
			var buttonAcept = new Button
			{
				Text = "Detalhes",
				Margin = 10,
				BackgroundColor = Color.Black,
				TextColor = Color.White,
				FontAttributes = FontAttributes.Bold
			};
			buttonAcept.Clicked += async (sender, e) =>
			{
				var order = Orders.Find((obj) => true);
				await Navigation.PushAsync(new OrderPage(order));
			};
			var buttonList = new Button
			{
				Text = "Ver Outras",
				Margin = 10,
				BackgroundColor = Color.Black,
				TextColor = Color.White,
				FontAttributes = FontAttributes.Bold
			};
			buttonList.Clicked += (sender, e) => Navigation.PushAsync(new OrdersPage(Orders)) ;
			var buttonSection = new Grid();
			buttonSection.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
			buttonSection.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
			buttonSection.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
			buttonSection.Children.Add(buttonAcept, 0, 0);
			buttonSection.Children.Add(buttonList, 1, 0);
			RadarFound = new StackLayout
			{
				Children =
				{
					headerFound, AddressLabel, ServiceLabel, buttonSection
				},
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				IsVisible = false
			};

			#endregion FoundRadar

			RadarGrid.Children.Add(RadarEmpty);
			RadarGrid.Children.Add(RadarLoading);
			RadarGrid.Children.Add(RadarFound);

			var washersRadar = new Frame
			{
				Content = RadarGrid
			};

			var moneyLabel = new Label
			{
				Text = "Financeiro",
				FontAttributes = FontAttributes.Bold,
				HorizontalOptions = LayoutOptions.Center
			};

			var money = new Frame
			{
				Content = new StackLayout
				{
					Children = {
						new Image { Source = "money.png", HeightRequest = 100, Aspect = Aspect.AspectFit},
						moneyLabel
					},
					HorizontalOptions = LayoutOptions.CenterAndExpand
				}
			};

			var tapGestureRecognizerMoney = new TapGestureRecognizer();
			tapGestureRecognizerMoney.Tapped += async (s, e) =>
			{
				await DisplayAlert("Atenção", "Menu disponível nas próximas versões", "OK");
			};
			money.GestureRecognizers.Add(tapGestureRecognizerMoney);

			var historicLabel = new Label
			{
				Text = "Agenda",
				FontAttributes = FontAttributes.Bold,
				HorizontalOptions = LayoutOptions.Center
			};

			var historic = new Frame
			{
				Content = new StackLayout
				{
					Children = {
						new Image { Source = "date.png", HeightRequest = 100, Aspect = Aspect.AspectFit},
						historicLabel
					},
					HorizontalOptions = LayoutOptions.CenterAndExpand
				}
			};
			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += async (s, e) =>
			{
				IsLoading = true;
				var orders = await WashesController.GetMyOrders();
				IsLoading = false;
				Navigation.PushAsync(new AgendaPage(orders));
			};
			historic.GestureRecognizers.Add(tapGestureRecognizer);

			var grid = new Grid();
			grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
			grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
			grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
			grid.Children.Add(money, 0, 0);
			grid.Children.Add(historic, 1, 0);

			return new ScrollView
			{
				Content = new StackLayout
				{
					Children = {
						header,
						new StackLayout {
							Padding = 15,
							Children = {
								washersRadar,
								grid
							}
						}
					}
				}
			};
		}

		void populateRadarFound()
		{
			if (Orders != null)
			{
				var firstOrder = Orders.Find((obj) => true);
				AddressLabel.Text = firstOrder.address.city.name + " - " + firstOrder.address.neighborhood;
				String servicesStr = "Serviços:";
				foreach (var orderItem in firstOrder.orderItems)
				{
					if (orderItem.service != null)
						servicesStr = servicesStr + "\n*" + orderItem.service.name;
				}
				ServiceLabel.Text = servicesStr;
			}
		}
	}
}


