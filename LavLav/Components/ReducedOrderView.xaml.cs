﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace LavLav
{
	public partial class ReducedOrderView : ContentView
	{
		public ReducedOrderView()
		{
			InitializeComponent();
		}
	}

	public class ReducedOrderCell : ViewCell
	{
		public ReducedOrderCell()
		{
			View = new ReducedOrderView();
		}
	}
}
