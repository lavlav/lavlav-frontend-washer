﻿using System;
using Xamarin.Forms;

namespace LavLav
{
	public class TitleFormView : ContentView
	{
		Label TitleLabel;

		public TitleFormView()
		{
			TitleLabel = new Label
			{
				Text = "Title",
				VerticalOptions = LayoutOptions.Center,
				TextColor = Constants.Colors.TextTitle
			};
			var layout = new StackLayout
			{
				Orientation = StackOrientation.Horizontal,
				BackgroundColor = Constants.Colors.BackgroudTitle,
				Padding = 10,
				Children = {
					TitleLabel
				}
			};
			Content = layout;
		}

		public string Title
		{
			get
			{
				return TitleLabel.Text;
			}

			set
			{
				TitleLabel.Text = value;
			}
		}
	}
}
