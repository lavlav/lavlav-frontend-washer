﻿using System;
using Xamarin.Forms;

namespace LavLav
{
	public class WashCell_old : ViewCell
	{
		public WashCell_old()
		{
			StackLayout cellWrapper = new StackLayout();
			Label title = new Label();
			Label description = new Label();

			title.SetBinding(Label.TextProperty, "StrNumber");
			description.SetBinding(Label.TextProperty, "Description");

			title.HorizontalOptions = LayoutOptions.Center;
			description.HorizontalOptions = LayoutOptions.Fill;
			title.FontSize = 20;
			title.FontAttributes = FontAttributes.Bold;

			cellWrapper.Children.Add(title);
			cellWrapper.Children.Add(description);

			View = cellWrapper;
		}


	}
}

