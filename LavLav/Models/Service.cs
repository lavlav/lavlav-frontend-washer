﻿using System;
namespace LavLav
{
	public class Service : BaseModel
	{
		public string name { get; set; }
		public string description { get; set; }
		public int duration { get; set; }
		public string icon { get; set; }
		public string createdAt { get; set; }
		public ServiceType serviceType { get; set; }
		public object price { get; set; }
	}

	public class ServiceType : BaseModel
	{
		public string name { get; set; }
		public string createdAt { get; set; }
	}
}
